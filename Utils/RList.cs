using System.Collections.Generic;
using Newtonsoft.Json;

namespace Quasar.Framework.Utils
{
    public class RList<T> where T: class {
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("data")]
        public List<T> Data { get; set; }
        [JsonProperty("total")]
        public int? Total { get; set; }

        public static RList<U> Ok<U>(List<U> data) where U: class {
            return new RList<U>(){
                Code = 200,
                Data = data,
                Total = data?.Count
            };
        } 
    }
}