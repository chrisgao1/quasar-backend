using System.Collections.Generic;
using Newtonsoft.Json;

namespace Quasar.Framework.Utils
{
    public class R<T> where T: class {
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("data")]
        public T Data { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }

        public static R<U> Ok<U>(U data, string message="OK") where U: class {
            return new R<U>(){
                Code = 20000,
                Data = data,
                Message = message
            };
        }

        public static R<U> Error<U>(int code, U data, string message) where U: class {
            return new R<U>(){
                Code = code,
                Data = data,
                Message = message
            };
        }  
    }
}