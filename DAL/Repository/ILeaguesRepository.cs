using Quasar.Framework.Entity;

namespace Quasar.Framework.DAL.Repository
{

    public interface ILeaguesRepository: IRepository<Leagues>
    {
        
    }
}