using Quasar.Framework.Entity;
using System.Linq;
using System.Linq.Expressions;
using System;
using System.Collections.Generic;

namespace Quasar.Framework.DAL.Repository
{

    public interface IUsersInfoRepository: IRepository<UsersInfo>
    {
        IEnumerable<UsersInfo> Query(Expression<Func<UsersInfo, bool>> expression);
    }
}