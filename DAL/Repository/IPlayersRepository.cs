using Quasar.Framework.Entity;
using System.Linq;
using System.Linq.Expressions;
using System;
using System.Collections.Generic;

namespace Quasar.Framework.DAL.Repository
{

    public interface IPlayersRepository: IRepository<Players>
    {
        IEnumerable<Players> Query(Expression<Func<Players, bool>> expression);
    }
}