using Quasar.Framework.Entity;

namespace Quasar.Framework.DAL.Repository
{

    public interface INewsRepository: IRepository<News>
    {
        
    }
}