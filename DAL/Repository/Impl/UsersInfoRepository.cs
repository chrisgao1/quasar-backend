using Quasar.Framework.Entity;
using Quasar.Framework.DAL.Db;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;
using System;
using System.Collections.Generic;

namespace Quasar.Framework.DAL.Repository.Impl;

public class UsersInfoRepository: Repository<UsersInfo>, IUsersInfoRepository {

    public UsersInfoRepository () {

    }

    
    public IEnumerable<UsersInfo> Query(Expression<Func<UsersInfo, bool>> expression){
        return this._objectContext.Set<UsersInfo>().Include(s=>s.User).Include(s=>s.Roles).Where(expression.Compile());
    }
}