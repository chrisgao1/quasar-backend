using Quasar.Framework.Entity;
using Quasar.Framework.DAL.Db;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;
using System;
using System.Collections.Generic;

namespace Quasar.Framework.DAL.Repository.Impl;

public class TeamsRepository: Repository<Teams>, ITeamsRepository {
    public TeamsRepository() {

    }

    public IEnumerable<Teams> Query(Expression<Func<Teams, bool>> expression){
        return this._objectContext.Set<Teams>().Include(s=>s.League).Where(expression.Compile());
    }
}