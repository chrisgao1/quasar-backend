using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Quasar.Framework.Extensions.Attributes;

namespace Quasar.Framework.DAL.Repository.Impl;

public class Repository<T> : IRepository<T> where T : class
{
    [Autowired]
    public DbContext _objectContext { get; set; }

    public IQueryable<T> Query()
    {
        return _objectContext.Set<T>();
    }

    public T FindByKey(params object[] keys)
    {
        return _objectContext.Set<T>().Find(keys);
    }

    public IQueryable<T> FuzzyQuery(Expression<Func<T, bool>> expression)
    {
        return _objectContext.Set<T>().Where(expression);
    }

    public bool Insert(T entity)
    {
        _objectContext.Set<T>().Attach(entity);
        _objectContext.Entry<T>(entity).State = EntityState.Added;
        return this.SaveChanges() > 0;
    }

    public bool Delete(T entity)
    {
        _objectContext.Entry<T>(entity).State = EntityState.Deleted;
        return this.SaveChanges() > 0;
    }

    public bool Update(T entity)
    {
        _objectContext.Entry<T>(entity).State = EntityState.Modified;
        return this.SaveChanges() > 0;
    }

    public void BeginTransaction()
    {
       if (_objectContext.Database.CurrentTransaction == null)
            _objectContext.Database.BeginTransaction();
    }

    public async Task<int> SaveChangesAsync()
    {
        return await _objectContext.SaveChangesAsync();
    }

    public int SaveChanges()
    {
        return _objectContext.SaveChanges();
    }

     public void Commit()
    {
        _objectContext.Database.CurrentTransaction?.Commit();
    }

    public void Rollback()
    {
        _objectContext.Database.CurrentTransaction?.Rollback();
    }
}