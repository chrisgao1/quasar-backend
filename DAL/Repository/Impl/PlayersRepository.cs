using Quasar.Framework.Entity;
using Quasar.Framework.DAL.Db;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Expressions;
using System;
using System.Collections.Generic;

namespace Quasar.Framework.DAL.Repository.Impl;

public class PlayersRepository: Repository<Players>, IPlayersRepository {

    public PlayersRepository () {

    }

    
    public IEnumerable<Players> Query(Expression<Func<Players, bool>> expression){
        return this._objectContext.Set<Players>().Include(s=>s.Team).ThenInclude(s=>s.League).Where(expression.Compile());
    }
}