using Quasar.Framework.Entity;

namespace Quasar.Framework.DAL.Repository
{

    public interface IUsersRepository: IRepository<Users>
    {
        
    }
}