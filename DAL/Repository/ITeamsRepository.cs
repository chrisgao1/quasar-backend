using Quasar.Framework.Entity;
using System.Linq;
using System.Linq.Expressions;
using System;
using System.Collections.Generic;

namespace Quasar.Framework.DAL.Repository
{

    public interface ITeamsRepository: IRepository<Teams>
    {
        IEnumerable<Teams> Query(Expression<Func<Teams, bool>> expression);
    }
}