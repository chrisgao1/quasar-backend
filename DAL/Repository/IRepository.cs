using System;
using System.Linq;
using System.Linq.Expressions;

namespace Quasar.Framework.DAL.Repository
{
   public interface IRepository<T> where T : class
   {
     T FindByKey(params object[] keys);
     IQueryable<T> Query();
     IQueryable<T> FuzzyQuery(Expression<Func<T, bool>> expression);
     bool Insert(T entity);
     bool Update(T entity);
     bool Delete(T entity);
   }
}