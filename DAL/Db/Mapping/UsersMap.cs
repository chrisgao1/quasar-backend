using Quasar.Framework.Entity; 
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Quasar.Framework.DAL.Db.Mapping
{

    public class UsersMap : IEntityTypeConfiguration<UsersInfo>
    {
        public void Configure(EntityTypeBuilder<UsersInfo> builder) {
            builder.HasOne(s=>s.User).WithOne(s=>s.UserInfo).HasForeignKey<UsersInfo>(s=>s.UserId);
            builder.HasMany(s=>s.Roles).WithMany(s=>s.UserInfos).UsingEntity<UsersRole>(s=>s.HasOne(s=>s.Role).WithMany().HasForeignKey(s=>s.RoleID), s=>s.HasOne(s=>s.User).WithMany().HasForeignKey(s=>s.UserID), s=>s.HasKey(t=>new {t.UserID, t.RoleID}));
        }
    }
}