using Quasar.Framework.Entity; 
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Quasar.Framework.DAL.Db.Mapping
{

    public class TeamsMap : IEntityTypeConfiguration<Teams>
    {
        public void Configure(EntityTypeBuilder<Teams> builder) {
            builder.HasMany(s=>s.Players).WithOne(s=>s.Team).HasForeignKey(s=>s.TeamID).IsRequired(true);
        }
    }
}