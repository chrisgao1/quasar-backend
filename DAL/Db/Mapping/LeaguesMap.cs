using Quasar.Framework.Entity; 
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Quasar.Framework.DAL.Db.Mapping
{

    public class LeaguesMap : IEntityTypeConfiguration<Leagues>
    {
        public void Configure(EntityTypeBuilder<Leagues> builder) {
            builder.HasMany(s=>s.Teams).WithOne(s=>s.League).HasForeignKey(s=>s.LeagueID).IsRequired(true);
        }
    }
}