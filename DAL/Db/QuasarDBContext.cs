using Microsoft.EntityFrameworkCore;
using Quasar.Framework.Entity;
using System.Reflection;
using Microsoft.Extensions.Logging;

namespace Quasar.Framework.DAL.Db
{
    public class QuasarDBContext : DbContext
    {
        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //     => optionsBuilder.UseNpgsql("Host=localhost;Database=my_db;Username=postgres;Password=admin");

        public QuasarDBContext(DbContextOptions<QuasarDBContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(LoggerFactory.Create(build =>
            {
                build.AddConsole();  // 用于控制台程序的输出
                build.AddDebug();    // 用于VS调试，输出窗口的输出
            }));
        }

        public DbSet<Players> Players { get; set; }

        public DbSet<Teams> Teams { get; set; }

        public DbSet<Leagues> Leagues { get; set; }

        public DbSet<Users> Users { get; set; }

        public DbSet<UsersInfo> UsersInfos { get; set; }

        public DbSet<Roles> Roles { get; set; }

        public DbSet<UsersRole> UsersRoles { get; set; }

        public DbSet<News> News { get; set; }

        public DbSet<PlayerData> PlayerData { get; set; }
    }
}