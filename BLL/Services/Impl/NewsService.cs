using Quasar.Framework.DAL.Repository;
using Quasar.Framework.Models;
using Quasar.Framework.Entity;
using Quasar.Framework.Utils;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System;

namespace Quasar.Framework.BLL.Services.Impl
{
    public class NewsService : BaseService, INewsService
    {  
        public INewsRepository repositiory { get; set; }

        public bool Insert(News news)
        {
            return repositiory.Insert(news);
        }

        public News QueryByID(params object[] keys)
        {
            News news = repositiory.FindByKey(keys);
            return news;
        }

        public List<News> QueryByExpression(News news)
        {
            Expression<Func<News, bool>> expression = PredicateBuilder.True<News>();
            if (news.ID !=default(Guid))
            {
                expression = expression.And(f => f.ID == news.ID);
            }
            if (!string.IsNullOrEmpty(news.Type))
            {
                expression = expression.And(f => f.Type.Equals(news.Type));
            }

            return repositiory.FuzzyQuery(expression).AsEnumerable().ToList();
        }
    }
}