using Quasar.Framework.DAL.Repository;
using Quasar.Framework.Models;
using Quasar.Framework.Entity;
using Quasar.Framework.Utils;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System;

namespace Quasar.Framework.BLL.Services.Impl
{
    public class PlayersService : BaseService, IPlayersService
    {  
        public IPlayersRepository repositiory { get; set; }

        public bool Insert(PlayersTO player)
        {
            return repositiory.Insert(mapper.Map<Players>(player));
        }

        public PlayersTO QueryByID(params object[] keys)
        {
            Players player = repositiory.FindByKey(keys);
            return mapper.Map<PlayersTO>(player);
        }

        public List<PlayersTO> QueryByExpression(PlayersTO playersTO)
        {
            Expression<Func<Players, bool>> expression = PredicateBuilder.True<Players>();
            if (playersTO.ID !=default(Guid))
            {
                expression = expression.And(f => f.ID == playersTO.ID);
            }
            if (!string.IsNullOrEmpty(playersTO.Team))
            {
                expression = expression.And(f => f.Team.Short.Equals(playersTO.Team));
            }
            if (!string.IsNullOrEmpty(playersTO.League))
            {
                expression = expression.And(f => f.Team.League.Name.Equals(playersTO.League));
            }

            var players = repositiory.Query(expression).ToList();
            return mapper.Map<List<PlayersTO>>(players);
        }
    }
}