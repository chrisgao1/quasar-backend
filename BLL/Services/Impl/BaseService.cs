using AutoMapper;

namespace Quasar.Framework.BLL.Services.Impl
{
    public class BaseService
    {
        public IMapper mapper { get; set; }
    }
}