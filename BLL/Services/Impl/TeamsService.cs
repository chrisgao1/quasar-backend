using Quasar.Framework.DAL.Repository;
using Quasar.Framework.Models;
using Quasar.Framework.Entity;
using Quasar.Framework.Utils;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System;

namespace Quasar.Framework.BLL.Services.Impl
{
    public class TeamsService : BaseService, ITeamsService
    {  
        public ITeamsRepository repositiory { get; set; }

        public bool Insert(TeamsTO team)
        {
            return repositiory.Insert(mapper.Map<Teams>(team));
        }

        public TeamsTO QueryByID(params object[] keys)
        {
            Teams team = repositiory.FindByKey(keys);
            return mapper.Map<TeamsTO>(team);
        }

        public List<TeamsTO> QueryByExpression(TeamsTO teamsTO)
        {
            Expression<Func<Teams, bool>> expression = PredicateBuilder.True<Teams>();
            if (teamsTO.ID !=default(Guid))
            {
                expression = expression.And(f => f.ID == teamsTO.ID);
            }
            if (!string.IsNullOrEmpty(teamsTO.League))
            {
                expression = expression.And(f => f.League.Name.Equals(teamsTO.League));
            }

            var teams = repositiory.Query(expression).ToList();
            return mapper.Map<List<TeamsTO>>(teams);
        }
    }
}