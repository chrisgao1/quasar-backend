using Quasar.Framework.DAL.Repository;
using Quasar.Framework.Models;
using Quasar.Framework.Entity;
using Quasar.Framework.Utils;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System;

namespace Quasar.Framework.BLL.Services.Impl
{
    public class PlayerDataService : BaseService, IPlayerDataService
    {  
        public IPlayerDataRepository repositiory { get; set; }

        public bool Insert(PlayerData playerData)
        {
            return repositiory.Insert(playerData);
        }

        public PlayerData QueryByID(params object[] keys)
        {
            PlayerData playerData = repositiory.FindByKey(keys);
            return playerData;
        }

        public List<PlayerData> QueryByExpression(PlayerData playerData)
        {
            Expression<Func<PlayerData, bool>> expression = PredicateBuilder.True<PlayerData>();
            if (playerData.ID !=default(Guid))
            {
                expression = expression.And(f => f.ID == playerData.ID);
            }
            if (!string.IsNullOrEmpty(playerData.Short))
            {
                expression = expression.And(f => f.Short.Equals(playerData.Short));
            }

            return repositiory.FuzzyQuery(expression).AsEnumerable().ToList();
        }
    }
}