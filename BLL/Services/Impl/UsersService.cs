using Quasar.Framework.DAL.Repository;
using Quasar.Framework.Models;
using Quasar.Framework.Entity;
using Quasar.Framework.Utils;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System;

namespace Quasar.Framework.BLL.Services.Impl
{
    public class UsersService : BaseService, IUsersService
    {  
        public IUsersRepository repositiory { get; set; }

        public bool Insert(Users user)
        {
            return repositiory.Insert(user);
        }

        public Users QueryByID(params object[] keys)
        {
            Users user = repositiory.FindByKey(keys);
            return user;
        }

        public Users QueryByExpression(Users user)
        {
            Expression<Func<Users, bool>> expression = PredicateBuilder.True<Users>();
            if (user.ID !=default(Guid))
            {
                expression = expression.And(f => f.ID == user.ID);
            }
            if (!string.IsNullOrEmpty(user.Username))
            {
                expression = expression.And(f => f.Username.Equals(user.Username));
            }
            if (!string.IsNullOrEmpty(user.Password))
            {
                expression = expression.And(f => f.Password.Equals(user.Password));
            }

            return repositiory.FuzzyQuery(expression).FirstOrDefault();
        }
    }
}