using Quasar.Framework.DAL.Repository;
using Quasar.Framework.Models;
using Quasar.Framework.Entity;
using Quasar.Framework.Utils;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System;

namespace Quasar.Framework.BLL.Services.Impl
{
    public class RolesService : BaseService, IRolesService
    {  
        public IRolesRepository repositiory { get; set; }

        public bool Insert(Roles role)
        {
            return repositiory.Insert(role);
        }

        public Roles QueryByID(params object[] keys)
        {
            Roles role = repositiory.FindByKey(keys);
            return role;
        }

        public List<Roles> QueryByExpression(Roles role)
        {
            Expression<Func<Roles, bool>> expression = PredicateBuilder.True<Roles>();
            if (role.ID !=default(Guid))
            {
                expression = expression.And(f => f.ID == role.ID);
            }
            if (!string.IsNullOrEmpty(role.Name))
            {
                expression = expression.And(f => f.Name.Equals(role.Name));
            }

            return repositiory.FuzzyQuery(expression).AsEnumerable().ToList();
        }
    }
}