using Quasar.Framework.DAL.Repository;
using Quasar.Framework.Models;
using Quasar.Framework.Entity;
using Quasar.Framework.Utils;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System;

namespace Quasar.Framework.BLL.Services.Impl
{
    public class UsersRoleService : BaseService, IUsersRoleService
    {  
        public IUsersRoleRepository repositiory { get; set; }

        public bool Insert(UsersRole user)
        {
            return repositiory.Insert(user);
        }

        public List<UsersRole> QueryByExpression(UsersRole usersRole)
        {
            Expression<Func<UsersRole, bool>> expression = PredicateBuilder.True<UsersRole>();
            if (usersRole.UserID !=default(Guid))
            {
                expression = expression.And(f => f.UserID == usersRole.UserID);
            }
            if (usersRole.RoleID !=default(Guid))
            {
                expression = expression.And(f => f.RoleID == usersRole.RoleID);
            }

            return repositiory.FuzzyQuery(expression).AsEnumerable().ToList();
        }
    }
}