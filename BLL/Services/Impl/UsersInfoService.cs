using Quasar.Framework.DAL.Repository;
using Quasar.Framework.Models;
using Quasar.Framework.Entity;
using Quasar.Framework.Utils;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System;

namespace Quasar.Framework.BLL.Services.Impl
{
    public class UsersInfoService : BaseService, IUsersInfoService
    {  
        public IUsersInfoRepository repositiory { get; set; }

        public bool Insert(UsersInfo user)
        {
            return repositiory.Insert(user);
        }

        public UsersInfoTO QueryByID(params object[] keys)
        {
            UsersInfo user = repositiory.FindByKey(keys);
            return mapper.Map<UsersInfoTO>(user);
        }

        public UsersInfoTO QueryByExpression(UsersInfoTO usersInfoTO)
        {
            Expression<Func<UsersInfo, bool>> expression = PredicateBuilder.True<UsersInfo>();
            if (usersInfoTO.ID !=default(Guid))
            {
                expression = expression.And(f => f.ID == usersInfoTO.ID);
            }
            if (!string.IsNullOrEmpty(usersInfoTO.Token))
            {
                expression = expression.And(f => f.User.Token.Equals(usersInfoTO.Token));
            }
            var users = repositiory.Query(expression).ToList().FirstOrDefault();
            return mapper.Map<UsersInfoTO>(users);
        }
    }
}