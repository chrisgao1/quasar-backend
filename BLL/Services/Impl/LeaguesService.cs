using Quasar.Framework.DAL.Repository;
using Quasar.Framework.Models;
using Quasar.Framework.Entity;
using Quasar.Framework.Utils;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System;

namespace Quasar.Framework.BLL.Services.Impl
{
    public class LeaguesService : BaseService, ILeaguesService
    {  
        public ILeaguesRepository repositiory { get; set; }

        public bool Insert(LeaguesTO league)
        {
            return repositiory.Insert(mapper.Map<Leagues>(league));
        }

        public LeaguesTO QueryByID(params object[] keys)
        {
            Leagues league = repositiory.FindByKey(keys);
            return mapper.Map<LeaguesTO>(league);
        }

        public List<LeaguesTO> QueryByExpression(LeaguesTO leaguesTO)
        {
            Expression<Func<Leagues, bool>> expression = PredicateBuilder.True<Leagues>();
            if (!string.IsNullOrEmpty(leaguesTO.Country))
            {
                expression.And(p => p.Country.Equals(leaguesTO.Country));
            }

            var leagues = repositiory.FuzzyQuery(expression).AsEnumerable().ToList();
            return mapper.Map<List<LeaguesTO>>(leagues);
        }
    }
}