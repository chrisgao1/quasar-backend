using Quasar.Framework.Entity;
using Quasar.Framework.Models;
using System.Collections.Generic;

namespace Quasar.Framework.BLL.Services
{
    public interface IRolesService
    {
        bool Insert(Roles user);

        Roles QueryByID(params object[] keys);

        List<Roles> QueryByExpression(Roles user);
    }
}