using Quasar.Framework.Entity;
using Quasar.Framework.Models;
using System.Collections.Generic;

namespace Quasar.Framework.BLL.Services
{
    public interface IPlayersService
    {   
        bool Insert(PlayersTO player);

        PlayersTO QueryByID(params object[] keys);

        List<PlayersTO> QueryByExpression(PlayersTO playersTO);
    }
}