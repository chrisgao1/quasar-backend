using Quasar.Framework.Entity;
using Quasar.Framework.Models;
using System.Collections.Generic;

namespace Quasar.Framework.BLL.Services
{
    public interface IUsersInfoService
    {
        bool Insert(UsersInfo user);

        UsersInfoTO QueryByID(params object[] keys);

        UsersInfoTO QueryByExpression(UsersInfoTO usersInfoTO);
    }
}