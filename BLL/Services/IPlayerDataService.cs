using Quasar.Framework.Entity;
using Quasar.Framework.Models;
using System.Collections.Generic;

namespace Quasar.Framework.BLL.Services
{
    public interface IPlayerDataService
    {   
        bool Insert(PlayerData playerData);

        PlayerData QueryByID(params object[] keys);

        List<PlayerData> QueryByExpression(PlayerData playerData);
    }
}