using Quasar.Framework.Entity;
using Quasar.Framework.Models;
using System.Collections.Generic;

namespace Quasar.Framework.BLL.Services
{
    public interface INewsService
    {   
        bool Insert(News news);

        News QueryByID(params object[] keys);

        List<News> QueryByExpression(News news);
    }
}