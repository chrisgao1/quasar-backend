using Quasar.Framework.Entity;
using Quasar.Framework.Models;
using System.Collections.Generic;

namespace Quasar.Framework.BLL.Services
{
    public interface ILeaguesService
    {   
        bool Insert(LeaguesTO league);

        LeaguesTO QueryByID(params object[] keys);

        List<LeaguesTO> QueryByExpression(LeaguesTO playersTO);
    }
}