using Quasar.Framework.Entity;
using Quasar.Framework.Models;
using System.Collections.Generic;

namespace Quasar.Framework.BLL.Services
{
    public interface IUsersService
    {
        bool Insert(Users user);

        Users QueryByID(params object[] keys);

        Users QueryByExpression(Users user);
    }
}