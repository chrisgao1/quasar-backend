using Quasar.Framework.Entity;
using Quasar.Framework.Models;
using System.Collections.Generic;

namespace Quasar.Framework.BLL.Services
{
    public interface IUsersRoleService
    {
        bool Insert(UsersRole user);

        List<UsersRole> QueryByExpression(UsersRole user);
    }
}