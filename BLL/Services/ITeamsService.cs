
using Quasar.Framework.Entity;
using Quasar.Framework.Models;
using System.Collections.Generic;

namespace Quasar.Framework.BLL.Services
{
    public interface ITeamsService
    {   
        bool Insert(TeamsTO player);

        TeamsTO QueryByID(params object[] keys);

        List<TeamsTO> QueryByExpression(TeamsTO playersTO);
    }
}