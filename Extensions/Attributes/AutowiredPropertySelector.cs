using System;
using System.Reflection;
using System.Linq;
using Autofac.Core;

namespace Quasar.Framework.Extensions.Attributes {
    
    public class AutowiredPropertySelector: IPropertySelector
    {
        public bool InjectProperty(PropertyInfo propertyInfo, object instance)
        {
            return propertyInfo.CustomAttributes.Any(it => it.AttributeType == typeof(AutowiredAttribute));
        }
    }
}