using System;

namespace Quasar.Framework.Extensions.Attributes {
    
    [AttributeUsage(AttributeTargets.Property)]
    public class AutowiredAttribute: Attribute {

    }
}