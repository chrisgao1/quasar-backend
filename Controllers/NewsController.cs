using System;
using System.Collections.Generic;
using Quasar.Framework.Models;
using Quasar.Framework.Entity;
using Quasar.Framework.Utils;
using Quasar.Framework.BLL.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Quasar.Framework.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NewsController : ControllerBase
    {
        private readonly ILogger<NewsController> _logger;

        public INewsService newsService { get; set;}

        public NewsController(ILogger<NewsController> logger)
        {
            _logger = logger;
        }

        [HttpGet("{id}")]
        public R<News> Get(Guid id)
        {
            var news = newsService.QueryByID(id);
            return R<object>.Ok(news);
        }

        [HttpGet("list")]
        public RList<News> Get([FromQuery]News news)
        {
           var data = newsService.QueryByExpression(news);
           return RList<object>.Ok(data);
        }

        [HttpPost("add")]
        public bool Insert([FromBody] News news){
            return newsService.Insert(news);
        }
    }
}
