using System;
using System.Collections.Generic;
using Quasar.Framework.Models;
using Quasar.Framework.Entity;
using Quasar.Framework.BLL.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Quasar.Framework.Utils;

namespace Quasar.Framework.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PlayersController : ControllerBase
    {
        private readonly ILogger<PlayersController> _logger;

        public IPlayersService playersService { get; set;}

        public IPlayerDataService playerDataService { get; set;}

        public PlayersController(ILogger<PlayersController> logger)
        {
            _logger = logger;
        }

        [HttpGet("{id}")]
        public R<PlayersTO> Get(Guid id)
        {
            var data = playersService.QueryByID(id);

            return R<object>.Ok(data);
        }

        [HttpGet("list/{team}")]
        public RList<PlayersTO> Get(string team, [FromQuery]PlayersTO players)
        {
            players.Team = team;
            var data = playersService.QueryByExpression(players);
            return RList<object>.Ok(data);
        }

        [HttpGet("list")]
        public RList<PlayersTO> Get([FromQuery]PlayersTO players)
        {
           var data = playersService.QueryByExpression(players);
           return RList<object>.Ok(data);
        }

        [HttpPost("add")]
        public bool Insert([FromBody] PlayersTO players){
            return playersService.Insert(players);
        }

        [HttpPost("data/add")]
        public bool Insert([FromBody] PlayerData playerData){
            return playerDataService.Insert(playerData);
        }

        [HttpGet("data")]
        public RList<PlayerData> Get([FromQuery]PlayerData playerData)
        {
           var data = playerDataService.QueryByExpression(playerData);
           return RList<object>.Ok(data);
        }
    }
}
