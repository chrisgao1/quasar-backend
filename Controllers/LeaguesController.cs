using System;
using System.Collections.Generic;
using Quasar.Framework.Models;
using Quasar.Framework.Utils;
using Quasar.Framework.BLL.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Quasar.Framework.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LeaguesController : ControllerBase
    {
        private readonly ILogger<LeaguesController> _logger;

        public ILeaguesService leaguesService { get; set;}

        public LeaguesController(ILogger<LeaguesController> logger)
        {
            _logger = logger;
        }

        [HttpGet("{id}")]
        public R<LeaguesTO> Get(Guid id)
        {
            var league = leaguesService.QueryByID(id);
            return R<object>.Ok(league);
        }

        [HttpGet("list")]
        public RList<LeaguesTO> Get([FromQuery]LeaguesTO leagues)
        {
           var data = leaguesService.QueryByExpression(leagues);
           return RList<object>.Ok(data);
        }

        [HttpPost("add")]
        public bool Insert([FromBody] LeaguesTO leagues){
            return leaguesService.Insert(leagues);
        }
    }
}
