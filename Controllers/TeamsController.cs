using System;
using System.Collections.Generic;
using Quasar.Framework.Models;
using Quasar.Framework.BLL.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Quasar.Framework.Utils;

namespace Quasar.Framework.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ILogger<TeamsController> _logger;

        public ITeamsService teamsService { get; set; }

        public TeamsController(ILogger<TeamsController> logger)
        {
            _logger = logger;
        }

        [HttpGet("{id}")]
        public R<TeamsTO> Get(Guid id)
        {
            var team = teamsService.QueryByID(id);
            return R<object>.Ok(team);
        }

        [HttpGet("list")]
        public RList<TeamsTO> Get([FromQuery]TeamsTO teams)
        {
           var data = teamsService.QueryByExpression(teams);
           return RList<object>.Ok(data);
        }

        [HttpPost("add")]
        public bool Insert([FromBody] TeamsTO teams){
            return teamsService.Insert(teams);
        }
    }
}
