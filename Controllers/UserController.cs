using System;
using System.Collections.Generic;
using Quasar.Framework.Models;
using Quasar.Framework.Entity;
using Quasar.Framework.Utils;
using Quasar.Framework.BLL.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Quasar.Framework.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;

        public IUsersService usersService { get; set;}

        public IUsersInfoService usersInfoService { get; set;}

        public IRolesService rolesService { get; set;}

        public IUsersRoleService usersRoleService { get; set;}

        public UserController(ILogger<UserController> logger)
        {
            _logger = logger;
        }

        [HttpPost("add")]
        public bool Insert([FromBody] Users user){
            return usersService.Insert(user);
        }

        [HttpPost("info/add")]
        public bool Insert([FromBody] UsersInfo user){
            return usersInfoService.Insert(user);
        }

        [HttpPost("role/add")]
        public bool Insert([FromBody] Roles role){
            return rolesService.Insert(role);
        }

        [HttpPost("userrole/add")]
        public bool Insert([FromBody] UsersRole role){
            return usersRoleService.Insert(role);
        }

        [HttpPost("login")]
        public R<Users> login([FromBody] Users user){
            var users = usersService.QueryByExpression(user);
            if (user == null) {
                return R<object>.Error(20001, users, "Invalid Credential");
            }
            return R<object>.Ok(users, "Login Successfully");
        }

        [HttpPost("info")]
        public R<UsersInfoTO> info([FromBody] UsersInfoTO user){
            var users = usersInfoService.QueryByExpression(user);
            if (user == null) {
                return R<object>.Error(20001, users, "Invalid Credential");
            }
            return R<object>.Ok(users, "Login Successfully");
        }
    }
}
