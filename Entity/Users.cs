using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace Quasar.Framework.Entity
{
    [Table("user", Schema = "quasarschema")]
    public class Users
    {
        [Key]
        [Column("id")]
        public Guid ID { get; set; }
        [Column("username")]
        public string Username { get; set; }
        [Column("token")]
        public string Token { get; set; }
        [Column("password")]
        public string Password { get; set; }
        public virtual UsersInfo UserInfo { get; set; }
    }
}