using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;

namespace Quasar.Framework.Entity
{
    [Table("userinfo", Schema = "quasarschema")]
    public class UsersInfo
    {
        [Key]
        [Column("id")]
        public Guid ID { get; set; }
        [Column("name")]
        public string Name { get; set; }
        [Column("introduction")]
        public string Introduction { get; set; }
        [Column("avatar")]
        public string Avatar { get; set; }
        public virtual List<Roles> Roles { get; set; }
        public virtual Users User { get; set; }
        [Column("userid")]
        public Guid UserId { get; set; }
    }
}