using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace Quasar.Framework.Entity
{
    [Table("user_role", Schema = "quasarschema")]
    public class UsersRole
    {
        [Column("userid")]
        public Guid UserID { get; set; }
        [Column("roleid")]
        public Guid RoleID { get; set; }

        public virtual UsersInfo User { get; set; }

        public virtual Roles Role { get; set; }
    }
}