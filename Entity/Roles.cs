using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;

namespace Quasar.Framework.Entity
{
    [Table("role", Schema = "quasarschema")]
    public class Roles
    {
        [Key]
        [Column("id")]
        public Guid ID { get; set; }
        [Column("name")]
        public string Name { get; set; }
        public virtual List<UsersInfo> UserInfos { get; set; }
    }
}