using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;

namespace Quasar.Framework.Entity 
{
    [Table("player-data", Schema = "quasarschema")]
    public class PlayerData {

        [Column("id")]
        public Guid ID { get; set; }
        [Column("name")]
        public string Name { get; set; }
        [Column("short")]
        public string Short { get; set; }
        [Column("year")]
        public int Year { get; set; }
        [Column("team")]
        public string Team { get; set; }
        [Column("league")]
        public string League { get; set; }
        [Column("score")]
        public double Score { get; set; }
        [Column("assist")]
        public double Assist { get; set; }
        [Column("fieldper")]
        public double Fieldper { get; set; }
        [Column("appearance")]
        public int Appearance { get; set; }
        [Column("time")]
        public double Time { get; set; }
    }
}