using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace Quasar.Framework.Entity
{
    [Table("player", Schema = "quasarschema")]
    public class Players
    {

        [Key]
        [Column("id")]
        public Guid ID { get; set; }
        [Column("name")]
        public string Name { get; set; }
        [Column("short")]
        public string Short { get; set; }
        [Column("nickname")]
        public string Nickname { get; set; }
        [Column("icon")]
        public string Icon { get; set; }
        [Column("team_id")]
        public Guid TeamID { get; set; }
        public virtual Teams Team { get; set; }
        [Column("age")]
        public int Age { get; set; }
        [Column("number")]
        public int Number { get; set; }
        [Column("nation")]
        public string Nation { get; set; }
        [Column("position")]
        public string Position { get; set; }
        [Column("rate")]
        public int Rate { get; set; }
        [Column("status")]
        public int Status { get; set; }
    }
}