using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;

namespace Quasar.Framework.Entity
{

    [Table("league", Schema = "quasarschema")]
    public class Leagues
    {

        [Key]
        [Column("id")]
        public Guid ID { get; set; }
        [Column("name")]
        public string Name { get; set; }
        [Column("country")]
        public string Country { get; set; }
        public virtual List<Teams> Teams { get; set; }
    }
}
