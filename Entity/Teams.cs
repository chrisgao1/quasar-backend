using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;

namespace Quasar.Framework.Entity
{
    [Table("team", Schema = "quasarschema")]
    public class Teams
    {
        [Key]
        [Column("id")]
        public Guid ID { get; set; }
        [Column("name")]
        public string Name { get; set; }
        [Column("short")]
        public string Short { get; set; }
        [Column("icon")]
        public string Icon { get; set; }
        [Column("city")]
        public string City { get; set; }
        [Column("league_id")]
        public Guid LeagueID { get; set; }
        public virtual Leagues League { get; set; }
        public virtual List<Players> Players { get; set; }
    }
}
