using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Quasar.Framework.Entity 
{
    [Table("news", Schema = "quasarschema")]
    public class News 
    {
        [Column("id")]
        public Guid ID { get; set; }
        [Column("timestamp")]
        public long Timestamp { get; set; }
        [Column("author")]
        public string Author { get; set; }
        [Column("reviewer")]
        public string Reviewer { get; set; }
        [Column("title")]
        public string Title { get; set; }
        [Column("content_short")]
        [JsonProperty("content_short")]
        public string ContentShort { get; set; }
        [Column("content")]
        public string Content { get; set; }
        [Column("importance")]
        public int Importance { get; set; }
        [Column("type")]
        public string Type { get; set; }
        [Column("status")]
        public string Status { get; set; }
        [Column("display_time")]
        [JsonProperty("display_time")]
        public DateTime DisplayTime { get; set; }
        [Column("comment_disabled")]
        [JsonProperty("comment_disabled")]
        public bool CommentDisabled { get; set; }
        [Column("pageviews")]
        public long PageViews { get; set; }
        [Column("image_uri")]
        [JsonProperty("image_uri")]
        public string ImageUri { get; set; }
    }
}