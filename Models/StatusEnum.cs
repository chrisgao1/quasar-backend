namespace Quasar.Framework.Models {
    
    public enum StatusEnum {
        Current = 0,
        Retired = 1
    }
}