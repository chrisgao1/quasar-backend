using Newtonsoft.Json;
using System;

namespace Quasar.Framework.Models
{
    public class TeamsTO
    {
        [JsonProperty("id")]
        public Guid ID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("short")]
        public string Short { get; set; }
        [JsonProperty("icon")]
        public string Icon { get; set; }
        [JsonProperty("leagueid")]
        public Guid LeagueID { get; set; }
        [JsonProperty("league")]
        public string League { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
    }
}
