using Newtonsoft.Json;
using System;

namespace Quasar.Framework.Models
{
    public class LeaguesTO
    {
        [JsonProperty("id")]
        public Guid ID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
    }
}
