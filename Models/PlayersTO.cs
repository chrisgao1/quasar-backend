using Newtonsoft.Json;
using System;

namespace Quasar.Framework.Models
{
    public class PlayersTO
    {
        [JsonProperty("id")]
        public Guid ID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("short")]
        public string Short { get; set; }
        [JsonProperty("nickname")]
        public string Nickname { get; set; }
        [JsonProperty("icon")]
        public string Icon { get; set; }
        [JsonProperty("teamid")]
        public Guid TeamID { get; set; }
        [JsonProperty("team")]
        public string Team { get; set; }
        [JsonProperty("league")]
        public string League { get; set; }
        [JsonProperty("age")]
        public int Age { get; set; }
        [JsonProperty("number")]
        public int Number { get; set; }
        [JsonProperty("nation")]
        public string Nation { get; set; }
        [JsonProperty("position")]
        public string Position { get; set; }
        [JsonProperty("rate")]
        public int Rate { get; set; }
        [JsonProperty("status")]
        public StatusEnum Status { get; set; }
    }
}
