using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Quasar.Framework.Models
{
    public class UsersInfoTO
    {
        [JsonProperty("id")]
        public Guid ID { get; set; }
        [JsonProperty("token")]
        public string Token { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("avatar")]
        public string Avatar { get; set; }
        [JsonProperty("introduction")]
        public string Introduction { get; set; }
        [JsonProperty("roles")]
        public List<string> roles { get; set; }
    }
}