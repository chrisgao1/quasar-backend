using Autofac;
using AutoMapper;
using System.Reflection;
using System;
using Microsoft.EntityFrameworkCore;
using Quasar.Framework.Extensions.Attributes;

namespace Quasar.Framework.Config
{

    public class AutofacModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Type baseRepoType = assembly.GetType("Quasar.Framework.DAL.Repository.Impl.Repository`1");
            builder.RegisterAssemblyTypes(assembly).Where(s=> s.BaseType.IsGenericType && s.BaseType.GetGenericTypeDefinition() == baseRepoType || baseRepoType.IsAssignableFrom(s)).AsImplementedInterfaces()
            .PropertiesAutowired(new AutowiredPropertySelector());
            Type baseServiceType = assembly.GetType("Quasar.Framework.BLL.Services.Impl.BaseService");
            builder.RegisterAssemblyTypes(assembly).Where(s => baseServiceType.IsAssignableFrom(s)).AsImplementedInterfaces()
            .PropertiesAutowired();
            Type contollerType = typeof(Microsoft.AspNetCore.Mvc.ControllerBase);
            builder.RegisterAssemblyTypes(assembly).Where(s => contollerType.IsAssignableFrom(s)).PropertiesAutowired();
            Type dbContextType = typeof(Microsoft.EntityFrameworkCore.DbContext);
            builder.RegisterAssemblyTypes(assembly).Where(s => dbContextType.IsAssignableFrom(s)).As<DbContext>().PropertiesAutowired().InstancePerDependency();
            builder.RegisterType<Mapper>().PropertiesAutowired();
        }
    }
}