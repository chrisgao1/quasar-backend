using AutoMapper;
using Quasar.Framework.Entity;
using Quasar.Framework.Models;
using System;

namespace Quasar.Framework.Mappers {
    public class TeamsMapper: Profile {
        public TeamsMapper()
        {
            // Mapping
            // 第一次参数是源类型（这里是Model类型），第二个参数是目标类型（这里是DTO类型）
            CreateMap<Teams, TeamsTO>().ForMember(d => d.League, opt =>
            {
                opt.MapFrom(s => s.League.Name);
            }).ForMember(d => d.LeagueID, opt =>
            {
                opt.MapFrom(s => s.League.ID);
            }).ForMember(d => d.Country, opt =>
            {
                opt.MapFrom(s => s.League.Country);
            }).ReverseMap()
            .ForPath(s => s.League, opt => opt.MapFrom(s => new Leagues(){ID=s.LeagueID}));
        }
    }
}