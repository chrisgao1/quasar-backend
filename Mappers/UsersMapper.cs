using AutoMapper;
using Quasar.Framework.Entity;
using Quasar.Framework.Models;
using System;
using System.Linq;

namespace Quasar.Framework.Mappers {
    public class UsersMapper: Profile {
        public UsersMapper()
        {
            // Mapping
            // 第一次参数是源类型（这里是Model类型），第二个参数是目标类型（这里是DTO类型）
            CreateMap<UsersInfo, UsersInfoTO>().ForMember(d => d.Token, opt =>
            {
                opt.MapFrom(s => s.User.Token);
            }).ForMember(d => d.roles, opt =>
            {
                opt.MapFrom(s => s.Roles.Select(p=>p.Name).ToList());
            });
        }
    }
}