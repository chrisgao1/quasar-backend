using AutoMapper;
using Quasar.Framework.Entity;
using Quasar.Framework.Models;
using System;

namespace Quasar.Framework.Mappers {
    public class LeaguesMapper: Profile {
        public LeaguesMapper()
        {
            // Mapping
            // 第一次参数是源类型（这里是Model类型），第二个参数是目标类型（这里是DTO类型）
            CreateMap<Leagues, LeaguesTO>().ReverseMap();
        }
    }
}