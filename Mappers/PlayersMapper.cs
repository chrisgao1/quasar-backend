using AutoMapper;
using Quasar.Framework.Entity;
using Quasar.Framework.Models;
using System;

namespace Quasar.Framework.Mappers {
    public class PlayersMapper: Profile {
        public PlayersMapper()
        {
            // Mapping
            // 第一次参数是源类型（这里是Model类型），第二个参数是目标类型（这里是DTO类型）
            CreateMap<Players, PlayersTO>().ForMember(d => d.Team, opt =>
            {
                opt.MapFrom(s => s.Team.Short);
            }).ForMember(d => d.League, opt =>
            {
                opt.MapFrom(s => s.Team.League.Name);
            }).ForMember(d=>d.Status, opt=> {
                opt.MapFrom(s => (StatusEnum)Enum.ToObject(typeof(StatusEnum), s.Status));
            }).ReverseMap().ForPath(s => s.Team, opt => opt.MapFrom(s => new Teams(){ID=s.TeamID}))
            .ForPath(s=>s.Status, opt=>opt.MapFrom(s => (int)s.Status));
        }
    }
}